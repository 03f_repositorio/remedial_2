from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import *
from .forms import jugadorforms, actujugadorforms, crearequipoforms, actuequipoforms, crearestadioforms, actuestadioforms



# Create your views here.


##J U G A D O R E S 
class listajugadores (generic.View):
    template_name = "core/jugador/listajugadores.html"
    context = {}

    def get(self, request, *args, **kwargs):
        jugador =  Jugador.objects.all()
        self.context = {
            "jugador": jugador
        }
        return render(request, self.template_name,self.context)
    
class crearjugador(generic.CreateView):
    template_name = "core/jugador/crearjugador.html"
    model = Jugador
    form_class = jugadorforms
    success_url = reverse_lazy("core:listajugadores")

class detallesjugador (generic.DetailView):
    template_name = "core/jugador/detallesjugador.html"
    model = Jugador

class actuajugador (generic.UpdateView):
    template_name = "core/jugador/actujugador.html"
    model = Jugador
    form_class = actujugadorforms
    success_url = reverse_lazy("core:listajugadores")

class borrarjugador (generic.DeleteView):
    template_name = "core/jugador/borrarjugador.html"
    model = Jugador
    success_url = reverse_lazy("core:listajugadores")
############## 

##E Q U I P O S 
class listaequipos (generic.View):
    template_name = "core/equipo/listaequipos.html"
    context = {}

    def get(self, request, *args, **kwargs):
        equipo =  Equipo.objects.all()
        self.context = {
            "equipos": equipo
        }
        return render(request, self.template_name,self.context)
    
class crearequipos(generic.CreateView):
    template_name = "core/equipo/crearequipo.html"
    model = Equipo
    form_class = crearequipoforms
    success_url = reverse_lazy("core:listaequipos")

class detallesquipo (generic.DetailView):
    template_name = "core/equipo/detallesequipo.html"
    model = Equipo

class actuequipo (generic.UpdateView):
    template_name = "core/equipo/actuequipo.html"
    model = Equipo
    form_class = actuequipoforms
    success_url = reverse_lazy("core:listaequipos")

class borrarequipo (generic.DeleteView):
    template_name = "core/equipo/borrarequipo.html"
    model = Equipo
    success_url = reverse_lazy("core:listaequipos")

############## 

## E S T A D I O S 
class listaestadios (generic.View):
    template_name = "core/estadio/listaestadios.html"
    context = {}

    def get(self, request, *args, **kwargs):
        estadio =  Estadio.objects.all()
        self.context = {
            "estadios": estadio
        }
        return render(request, self.template_name,self.context)
    
class crearestadio(generic.CreateView):
    template_name = "core/estadio/crearestadio.html"
    model = Estadio
    form_class = crearestadioforms
    success_url = reverse_lazy("core:listaestadios")

class detallesestadio (generic.DetailView):
    template_name = "core/estadio/detallesestadio.html"
    model = Estadio

class actuestadio (generic.UpdateView):
    template_name = "core/estadio/actuestadio.html"
    model = Estadio
    form_class = actuestadioforms
    success_url = reverse_lazy("core:listaestadios")

class borrarestadio (generic.DeleteView):
    template_name = "core/estadio/borrarestadio.html"
    model = Estadio
    success_url = reverse_lazy("core:listaestadios")

