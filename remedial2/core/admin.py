from django.contrib import admin
from .models import Equipo, Jugador, Estadio 

# Register your models here.

@admin.register(Equipo)
class estAdmin(admin.ModelAdmin):
    list_display = [
        "nombre",
        "evento",
        "propietario",
    ]

@admin.register(Jugador)
class estAdmin(admin.ModelAdmin):
    list_display = [
        "nombre",
        "equipo",
        "posicion",
        "numero",
    ]

@admin.register(Estadio)
class estAdmin(admin.ModelAdmin):
    list_display = [
        "nombre",
        "capacidad",
        "espacio",
        "equipos",
    ]
  