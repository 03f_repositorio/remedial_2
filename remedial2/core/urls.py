from django.urls import path
from core import views

app_name="core"
urlpatterns = [

    ##urls jugador
    path('lista/jugador/',views.listajugadores.as_view(), name="listajugadores"),
    path('crear/jugador/',views.crearjugador.as_view(), name="crearjugador"),
    path('detalles/jugador/<int:pk>/', views.detallesjugador.as_view(), name= "detallesjugador"),
    path('actualizar/jugador/<int:pk>/', views.actuajugador.as_view(), name= "actujugador"),
    path('borrar/jugador/<int:pk>/', views.borrarjugador.as_view(), name= "borrarjugador"),

    ##urls equipos
    path('lista/equipo/',views.listaequipos.as_view(), name="listaequipos"),
    path('crear/equipo/',views.crearequipos.as_view(), name="crearequipo"),
    path('detalles/equipo/<int:pk>/', views.detallesquipo.as_view(), name= "detallesequipo"),
    path('actualizar/equipo/<int:pk>/', views.actuequipo.as_view(), name= "actuequipo"),
    path('borrar/equipo/<int:pk>/', views.borrarequipo.as_view(), name= "borrarequipo"),

    ##urls estadios
    path('lista/estadio/',views.listaestadios.as_view(), name="listaestadios"),
    path('crear/estadio/',views.crearestadio.as_view(), name="crearestadio"),
    path('detalles/estadio/<int:pk>/', views.detallesestadio.as_view(), name= "detallesestadio"),
    path('actualizar/estadio/<int:pk>/', views.actuestadio.as_view(), name= "actuestadio"),
    path('borrar/estadio/<int:pk>/', views.borrarestadio.as_view(), name= "borrarestadio"),
]