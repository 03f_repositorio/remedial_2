from django import forms
from .models import *

##jugador
class jugadorforms (forms.ModelForm):
    class Meta:
        model = Jugador
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del jugador"}),
            "equipos": forms.Select(attrs={"class":"form-select form-control"}),
            "posicion": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Posicion del jugador"}),
            "numero":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero del jugador"}),
        }


class actujugadorforms (forms.ModelForm):
    class Meta:
        model = Jugador
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del jugador"}),
            "equipos": forms.Select(attrs={"class":"form-select form-control"}),
            "posicion": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Posicion del jugador"}),
            "numero":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero del jugador"}),
        }
        
#Equipo 
class crearequipoforms (forms.ModelForm):
    class Meta:
        model = Equipo
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo"}),
            "evento":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Evento"}),
            "propietario": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre del propietario"}),
            }



class actuequipoforms (forms.ModelForm):
    class Meta:
        model = Equipo
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo"}),
            "evento":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Evento"}),
            "propietario": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre del propietario"}),
            }



#Estadio 
class crearestadioforms (forms.ModelForm):
    class Meta:
        model = Equipo
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre del estadio"}),
            "capacidad":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Capacidad"}),
            "espacio":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Capacidad"}),

            }



class actuestadioforms (forms.ModelForm):
    class Meta:
        model = Equipo
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "equipos": forms.Select(attrs={"class":"form-select form-control"}),
            "equipos": forms.Select(attrs={"class":"form-select form-control"}),
            }


