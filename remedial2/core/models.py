from django.db import models

class Equipo(models.Model):
    nombre = models.CharField(max_length=50)
    evento = models.CharField(max_length=254)
    propietario = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nombre 


class Jugador (models.Model):
    nombre = models.CharField(max_length=50)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)
    posicion = models.CharField(max_length=50)
    numero = models.IntegerField()
    def __str__(self):
        return self.nombre 
    
class Estadio(models.Model): 
    nombre = models.CharField(max_length=50)
    capacidad = models.IntegerField()
    espacio = models.CharField
    equipos=models.ForeignKey(Equipo, on_delete=models.CASCADE)
    def __str__(self):
        return self.nombre 

